#ifndef INCLUDE_BUTTON_HPP
#define INCLUDE_BUTTON_HPP

#include "Listener.hpp"
#include "ResourceManager.hpp"
#include "InputMessage.hpp"
#include <SFML/Graphics.hpp>
#include <functional>

namespace cn
{
	class Button : public Listener
	{
		sf::RectangleShape shape;
		std::shared_ptr<sf::Texture> normalTexture;
		std::shared_ptr<sf::Texture> hoverTexture;
		std::shared_ptr<sf::Texture> activeTexture;
		std::shared_ptr<sf::Font> font;
		sf::Text text;

		float hoverModifier{ 0.8f };
		float activeModifier{ 0.6f };
		sf::Color defaultColor{ 128, 128, 128, 255 };

		sf::Vector2f position;
		sf::Vector2f size;

		TextureManager& textureManager;
		ResourceManager<sf::Font>& fontManager;
		std::function<void()> callback;

		virtual void handleInputMessage(std::shared_ptr<InputMessage>);
	public:
		Button(sf::Vector2f, sf::Vector2f, std::string, TextureManager&, ResourceManager<sf::Font>&,
				std::string, std::string, std::string, std::string);
		Button(sf::Vector2f, sf::Vector2f, std::string, TextureManager&, ResourceManager<sf::Font>&,
				std::string);
		~Button();
		virtual void handleMessage(std::shared_ptr<Message>) override;
		void draw(sf::RenderTarget& rt);
		void registerCallback(std::function<void()>);
		void unregisterCallback();

		void setNormalTexture(std::string);
		void setHoverTexture(std::string);
		void setActiveTexture(std::string);

		void resetNormalTexture();
		void resetHoverTexture();
		void resetActiveTexture();

		void setHoverColorModifier(float);
		void setActiveColorModifier(float);
		void setDefaultColor(sf::Color);

		float getHoverColorModifier() const;
		float getActiveColorModifier() const;
		sf::Color getDefaultColor() const;

	};
}

#endif // !INCLUDE_BUTTON_HPP
