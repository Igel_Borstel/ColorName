#ifndef INCLUDE_LISTENER_HPP
#define INCLUDE_LISTENER_HPP

#include <memory>
#include "messages/Message.hpp"

namespace cn
{
	struct Listener
	{
		virtual void handleMessage(std::shared_ptr<Message>) = 0;
	};
}

#endif // !INCLUDE_LISTENER_HPP

