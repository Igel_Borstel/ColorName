#ifndef INCLUDE_MESSAGES_MESSAGE_HPP
#define INCLUDE_MESSAGES_MESSAGE_HPP

namespace cn
{
	struct Message
	{
		virtual ~Message() = default;
	};
}

#endif // !INCLUDE_MESSAGES_MESSAGE_HPP
