#ifndef INCLUDE_MESSAGESENDER_HPP
#define INCLUDE_MESSAGESENDER_HPP


#include <memory>
#include <map>
#include "messages/Message.hpp"
#include "Listener.hpp"
#include <string>

namespace cn
{
	class MessageSender
	{
		std::multimap<std::string, std::shared_ptr<Listener>> listenerMap;
		std::multimap<std::shared_ptr<Listener>, std::multimap<std::string, std::shared_ptr<Listener>>::iterator> listenerAddressMap;
	public:
		void registerListener(std::shared_ptr<Listener>, std::unique_ptr<Message>);
		void registerListener(std::shared_ptr<Listener>, std::string);
		void unregisterListener(std::shared_ptr<Listener>);
		void send(std::shared_ptr<Message>) const;
		bool isRegistered(std::shared_ptr<Listener>) const;
	};
}

#endif // !INCLUDE_EVENTMANAGER_HPP
