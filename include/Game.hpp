#ifndef INCLUDE_GAME_HPP
#define INCLUDE_GAME_HPP

#include <SFML/Graphics.hpp>
#include "MessageSender.hpp"
#include "Logger.hpp"
#include "ResourceManager.hpp"
#include "State.hpp"
#include "Button.hpp"

namespace cn
{
	class Game
	{
		sf::RenderWindow window;
		MessageSender& sender;
		Logger& logger;
		ResourceManager<sf::Font>& fontManager;
		TextureManager& textureManager;
		//std::shared_ptr<State> currentState;

		std::shared_ptr<Button> button;
	public:
		Game(MessageSender&, Logger&, ResourceManager<sf::Font>&, TextureManager&);
		void loop();
		void translateEvents();
	};
}

#endif // !INCLUDE_GAME_HPP
