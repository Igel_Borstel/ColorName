#ifndef INCLUDE_MESSAGES_INPUTMESSAGE_HPP
#define INCLUDE_MESSAGES_INPUTMESSAGE_HPP

#include "messages/Message.hpp"
#include <SFML/System.hpp>

namespace cn
{
	class InputMessage : public Message
	{
	public:
		enum class Type
		{
			touchBegan = 0,
			touchEnded,
			touchMoved
		};
	private:
		Type type;
		sf::Vector2f position;
	public:
		InputMessage(Type, sf::Vector2f);
		Type getType() const;
		sf::Vector2f getPosition() const;
	};

	inline InputMessage::InputMessage(InputMessage::Type typ, sf::Vector2f pos)
		: type(typ), position(pos)
	{}

	inline InputMessage::Type InputMessage::getType() const {	return type; }
	inline sf::Vector2f InputMessage::getPosition() const { return position; }
}
#endif // !INCLUDE_MESSAGES_INPUTMESSAGE_HPP
