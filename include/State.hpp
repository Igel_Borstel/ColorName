#ifndef INCLUDE_STATE_HPP
#define INCLUDE_STATE_HPP

#include <string>
#include "Listener.hpp"
#include "Game.hpp"
#include <SFML/Graphics.hpp>

namespace cn
{
	struct State : public Listener
	{
		virtual ~State() = default;

		virtual void pause() = 0;
		virtual void resume() = 0;

		virtual void update(sf::Time) = 0;
		virtual void draw(sf::RenderTarget&) = 0;

		//virtual void changeState();
		//Game& game;
	};
}

#endif // !INCLUDE_STATE_HPP
