#ifndef INCLUDE_WELCOMESTATE_HPP
#define INCLUDE_WELCOMESTATE_HPP

#include "State.hpp"
#include "InputMessage.hpp"

namespace cn
{
	class WelcomeState : public State
	{
		void handleInputMessage(std::shared_ptr<InputMessage>);
	public:
		virtual ~WelcomeState() override;

		virtual void pause() override;
		virtual void resume() override;

		virtual void update(sf::Time) override;
		virtual void draw(sf::RenderTarget&) override;

		virtual void handleMessage(std::shared_ptr<Message>) override;
	};
}

#endif // !INCLUDE_WELCOMESTATE_HPP
