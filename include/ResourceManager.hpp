#ifndef INCLUDE_RESOURCE_MANAGER_HPP
#define INCLUDE_RESOURCE_MANAGER_HPP

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <memory>
#include <map>
#include <string>
#include <stdexcept>
#include <sstream>

namespace cn
{

	template<class T>
	class ResourceManager
	{
		std::map<std::string, std::shared_ptr<T>> resourceMap;
	public:
		
		void loadResource(std::string name, std::string path)
		{
			if (resourceMap.insert(std::pair<std::string, std::shared_ptr<T>>{name, std::make_shared<T>()}).second)
			{
				if (!resourceMap.at(name)->loadFromFile(path))
				{
					std::stringstream errmsg;
					errmsg << "Error during resource loading! File does not exists or could not opened. File: " << path;
					throw std::runtime_error(errmsg.str());
				}
			}
			else
			{
				std::stringstream errmsg;
				errmsg << "Error during resource loading! Name is already used! Name: " << name;
				throw std::logic_error(errmsg.str());
			}
		}
		
		void unloadResource(std::string name)
		{
			auto iterator = resourceMap.find(name);
			if (iterator != resourceMap.end())
			{
				if (resourceMap.at(iterator).use_count() == 1)
				{
					resourceMap.erase(iterator);
				}
			}
		}
		
		std::shared_ptr<T> getResource(std::string name)
		{
			try
			{
				return resourceMap.at(name);
			}
			catch (std::out_of_range except)
			{
				return nullptr;
			}
		}

		void addResource(std::string name, std::shared_ptr<T> resource)
		{
			if (!resourceMap.insert(std::pair<std::string, std::shared_ptr<T>>{ name, resource }).second)
			{
				std::stringstream errmsg;
				errmsg << "Error during resource loading! Name is already used! Name: " << name;
				throw std::logic_error(errmsg.str());
			}
		}
	};

	using ImageManager = ResourceManager<sf::Image>;
	using TextureManager = ResourceManager<sf::Texture>;
}

#endif // !INCLUDE_RESOURCE_MANAGER_HPP
