#include "../include/Button.hpp"

#include <iostream>

namespace cn
{
	void Button::handleInputMessage(std::shared_ptr<InputMessage> msg)
	{
		if (msg->getPosition().x >= position.x && msg->getPosition().x <= position.x + size.x &&
			msg->getPosition().y >= position.y && msg->getPosition().y <= position.y + size.y)
		{
			//TODO weitere Aktionen durchführen
			switch (msg->getType())
			{
			case InputMessage::Type::touchBegan:
				if (activeTexture != nullptr)
				{
					shape.setFillColor(sf::Color{ 255, 255, 255, 255 });
					shape.setTexture(activeTexture.get());
				}
				else
				{
					shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(defaultColor.r * activeModifier), static_cast<sf::Uint8>(defaultColor.g * activeModifier), static_cast<sf::Uint8>(defaultColor.b * activeModifier) });
				}
				shape.setSize(size);
				break;
			case InputMessage::Type::touchEnded:
				if (hoverTexture != nullptr)
				{
					shape.setFillColor(sf::Color{ 255, 255, 255, 255 });
					shape.setTexture(hoverTexture.get());
				}
				else
				{
					shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(defaultColor.r * hoverModifier), static_cast<sf::Uint8>(defaultColor.g * hoverModifier), static_cast<sf::Uint8>(defaultColor.b * hoverModifier) });
				}
				shape.setSize(size);
				if (callback)
				{
					callback();
				}
				break;
			case InputMessage::Type::touchMoved:
				if (sf::Touch::isDown(0) || sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					if (activeTexture != nullptr)
					{
						shape.setFillColor(sf::Color{ 255, 255, 255, 255 });
						shape.setTexture(activeTexture.get());
					}
					else
					{
						shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(defaultColor.r * activeModifier), static_cast<sf::Uint8>(defaultColor.g * activeModifier), static_cast<sf::Uint8>(defaultColor.b * activeModifier) });
					}
					shape.setSize(size);
				}
				else
				{
					if (hoverTexture != nullptr)
					{
						shape.setFillColor(sf::Color{ 255, 255, 255, 255 });
						shape.setTexture(hoverTexture.get());
					}
					else
					{
						shape.setFillColor(sf::Color{ static_cast<sf::Uint8>(defaultColor.r * hoverModifier), static_cast<sf::Uint8>(defaultColor.g * hoverModifier), static_cast<sf::Uint8>(defaultColor.b * hoverModifier) });
					}
					shape.setSize(size);
				}
				break;
			}
		}
		else
		{
			if (activeTexture != nullptr)
			{
				shape.setFillColor(sf::Color{ 255, 255, 255, 255 });
				shape.setTexture(normalTexture.get());
			}
			else
			{
				shape.setFillColor(defaultColor);
			}
			shape.setSize(size);
		}
	}

	Button::Button(sf::Vector2f pos, sf::Vector2f sz, std::string txt, TextureManager& tm, ResourceManager<sf::Font>& fm,
		std::string fontName, std::string normalName, std::string hoverName, std::string activeName)
		: position{ pos }, size{ sz }, textureManager{ tm }, fontManager{ fm }
	{
		if (!normalName.empty())
		{
			normalTexture = textureManager.getResource(normalName);
			shape.setFillColor(defaultColor);
		}
		if (!hoverName.empty())
		{
			hoverTexture = textureManager.getResource(hoverName);
		}
		if (!activeName.empty())
		{
			activeTexture = textureManager.getResource(activeName);
		}
		font = fontManager.getResource(fontName);

		shape.setPosition(position);
		if (normalTexture != nullptr)
		{
			shape.setTexture(normalTexture.get());
		}
		else
		{
			shape.setFillColor(defaultColor);
		}
		shape.setSize(size);

		text.setString(txt);
		text.setFont(*font);
		text.setPosition(position);
		while (text.getLocalBounds().width > size.x || text.getLocalBounds().height > size.y)
		{
			text.setCharacterSize(text.getCharacterSize() - 1);
		}
	}

	Button::Button(sf::Vector2f pos, sf::Vector2f sz, std::string txt, TextureManager& tm, ResourceManager<sf::Font>& fm,
		std::string fontName)
		: Button(pos, sz, txt, tm, fm, fontName, "", "", "")
	{}

	Button::~Button()
	{
		//TODO Button als Listener deregisterieren
	}

	void Button::handleMessage(std::shared_ptr<Message> msg)
	{
		if (typeid(*msg) == typeid(InputMessage))
		{
			handleInputMessage(std::dynamic_pointer_cast<InputMessage>(msg));
		}
	}

	void Button::draw(sf::RenderTarget& rt)
	{
		shape.setPosition(position);
		text.setPosition(position);
		rt.draw(shape);
		rt.draw(text);
	}

	void Button::registerCallback(std::function<void()> cb)
	{
		callback = cb;
	}

	void Button::unregisterCallback()
	{
		if (callback)
		{
			callback = nullptr;
		}
	}

	void Button::setNormalTexture(std::string name)
	{
		normalTexture = textureManager.getResource(name);
	}

	void Button::setHoverTexture(std::string name)
	{
		hoverTexture = textureManager.getResource(name);
	}

	void Button::setActiveTexture(std::string name)
	{
		activeTexture = textureManager.getResource(name);
	}

	void Button::resetNormalTexture()
	{
		normalTexture = nullptr;
	}

	void Button::resetHoverTexture()
	{
		hoverTexture = nullptr;
	}

	void Button::resetActiveTexture()
	{
		activeTexture = nullptr;
	}

	void Button::setHoverColorModifier(float mod)
	{
		hoverModifier = mod;
	}

	void Button::setActiveColorModifier(float mod)
	{
		activeModifier = mod;
	}

	void Button::setDefaultColor(sf::Color color)
	{
		defaultColor = color;
	}

	float Button::getHoverColorModifier() const
	{
		return hoverModifier;
	}

	float Button::getActiveColorModifier() const
	{
		return activeModifier;
	}

	sf::Color Button::getDefaultColor() const
	{
		return defaultColor;
	}

}