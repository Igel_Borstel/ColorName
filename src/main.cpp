#include <SFML/Graphics.hpp>
#include "../include/Game.hpp"
#include "../include/ResourceManager.hpp"
int main()
{
	cn::Logger logger;
	cn::MessageSender sender;
	cn::ResourceManager<sf::Font> fontManager;
	cn::TextureManager textureManager;

	fontManager.loadResource("gi_reg", "resources/fonts/GlacialIndifference-Regular.otf");

	textureManager.loadResource("buttonNormal", "resources/gfx/ui/button.png");
	textureManager.loadResource("buttonHover", "resources/gfx/ui/button_hover.png");
	textureManager.loadResource("buttonActive", "resources/gfx/ui/button_active.png");
	cn::Game game{ sender, logger, fontManager, textureManager};
	game.loop();
}