#include <typeinfo>
#include "../include/MessageSender.hpp"

namespace cn
{
	void MessageSender::registerListener(std::shared_ptr<Listener> listener, std::unique_ptr<Message> msg)
	{
		listenerAddressMap.insert(std::pair<std::shared_ptr<Listener>, std::multimap<std::string, std::shared_ptr<Listener>>::iterator>(listener, listenerMap.insert(std::pair<std::string, std::shared_ptr<Listener>>(typeid(*msg).name(), listener))));
	}

	void MessageSender::registerListener(std::shared_ptr<Listener> listener, std::string msgtype)
	{
		listenerAddressMap.insert(std::pair<std::shared_ptr<Listener>, std::multimap<std::string, std::shared_ptr<Listener>>::iterator>(listener, listenerMap.insert(std::pair<std::string, std::shared_ptr<Listener>>(msgtype, listener))));
	}

	void MessageSender::unregisterListener(std::shared_ptr<Listener> listener)
	{
		auto pairs = listenerAddressMap.equal_range(listener);
		for (auto iterator = pairs.first; iterator != pairs.second; ++iterator)
		{
			listenerMap.erase(iterator->second);
		}
		listenerAddressMap.erase(listener);
	}

	void MessageSender::send(std::shared_ptr<Message> msg) const
	{
		auto pairs = listenerMap.equal_range(typeid(*msg).name());
		for (auto iterator = pairs.first; iterator != pairs.second; ++iterator)
		{
			iterator->second->handleMessage(msg);
		}
	}

	bool MessageSender::isRegistered(std::shared_ptr<Listener> listener) const
	{
		return (listenerAddressMap.find(listener) != listenerAddressMap.end()) ? true : false;
	}
}