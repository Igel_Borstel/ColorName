#include "../include/Game.hpp"
#include "../include/InputMessage.hpp"

#include <iostream>

namespace cn
{
	Game::Game(MessageSender& send, Logger& log, ResourceManager<sf::Font>& fontmngr, TextureManager& texturemngr)
		: sender{ send }, logger{ log }, fontManager{ fontmngr }, textureManager{ texturemngr }
	{
		window.create(sf::VideoMode{ 600, 300 }, "ColorName - Testspiel");
		//button = std::make_shared<Button>(sf::Vector2f{ static_cast<float>(window.getSize().x) / 2, static_cast<float>(window.getSize().y) / 2 }, sf::Vector2f{ 160, 60 }, "Testbutton", textureManager, fontManager, "gi_reg", "buttonNormal", "buttonHover", "buttonActive");
		button = std::make_shared<Button>(sf::Vector2f{ static_cast<float>(window.getSize().x) / 2, static_cast<float>(window.getSize().y) / 2 }, sf::Vector2f{ 160, 60 }, "Testbutton", textureManager, fontManager, "gi_reg");
		sender.registerListener(button, typeid(InputMessage).name());
		button->registerCallback([&]() {
			std::cout << "Unregister Callback!" << std::endl;
			button->unregisterCallback(); });
	}

	void Game::loop()
	{
		while (window.isOpen())
		{
			translateEvents();
			window.clear(sf::Color::Blue);
			button->draw(window);
			window.display();
		}
	}

	void Game::translateEvents()
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::TouchBegan:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchBegan, sf::Vector2f{ static_cast<float>(sf::Touch::getPosition(0, window).x), static_cast<float>(sf::Touch::getPosition(0, window).y) }));
				break;
			case sf::Event::MouseButtonPressed:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchBegan, sf::Vector2f{ static_cast<float>(sf::Mouse::getPosition(window).x), static_cast<float>(sf::Mouse::getPosition(window).y) }));
				break;
			case sf::Event::TouchEnded:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchEnded, sf::Vector2f{ static_cast<float>(sf::Touch::getPosition(0, window).x), static_cast<float>(sf::Touch::getPosition(0, window).y) }));
				break;
			case sf::Event::MouseButtonReleased:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchEnded, sf::Vector2f{ static_cast<float>(sf::Mouse::getPosition(window).x), static_cast<float>(sf::Mouse::getPosition(window).y) }));
				break;
			case sf::Event::TouchMoved:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchMoved, sf::Vector2f{ static_cast<float>(sf::Touch::getPosition(0, window).x), static_cast<float>(sf::Touch::getPosition(0, window).y) }));
				break;
			case sf::Event::MouseMoved:
				sender.send(std::make_shared<InputMessage>(InputMessage::Type::touchMoved, sf::Vector2f{ static_cast<float>(sf::Mouse::getPosition(window).x), static_cast<float>(sf::Mouse::getPosition(window).y) }));
				break;
			}
		}
	}
}